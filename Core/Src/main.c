/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stdbool.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
int LED1,LED2,LED3,LED4,LED5,LED6,BUT;
int counter=0, speed=0;


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{


  bool BUTCONTROLSTATE=false;   /* CONTROL PUSH BUTTON STATUS */
  bool start=false;      /* STOP OR INIT SEQUENCE */

  HAL_Init();


  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
/* ****************************** PUSH BUTTON********************* */
/* this condition will only increase one each time the button is pressed */

 BUT=HAL_GPIO_ReadPin(BUTTON_GPIO_Port,BUTTON_Pin);

	  if (BUT && !BUTCONTROLSTATE){
		  counter++;
		  BUTCONTROLSTATE=true;

	  }
	  else if(!BUT && BUTCONTROLSTATE ){
		 BUTCONTROLSTATE=false;
	  }
/* *************************************************************** */

/* ***************************** ****STEPS ************************ */
/*   This steeps will put the values to speed, stop and start  **** */

	  switch(counter){
	  case 0: start=false;
	  	  break;
	  case 1: start=true;
	  	  	  speed=100;
	  	  break;
	  case 2: speed=50;
	  	  break;
	  case 3: speed=20;
	  	  break;
	  case 4: start=false;
	  	  	  counter=0;
	  	  break;

	  }

/* ************************************* LED SEQUENCE ***************** */

	  if (start){

		  HAL_GPIO_TogglePin(LED_3_GPIO_Port,LED_3_Pin);
		  LED3=HAL_GPIO_ReadPin(LED_3_GPIO_Port, LED_3_Pin);
		  HAL_GPIO_TogglePin(LED_4_GPIO_Port,LED_4_Pin);
		  LED4=HAL_GPIO_ReadPin(LED_4_GPIO_Port, LED_4_Pin);
		  HAL_Delay(speed);
		  HAL_GPIO_TogglePin(LED_2_GPIO_Port,LED_2_Pin);
		  LED2=HAL_GPIO_ReadPin(LED_2_GPIO_Port, LED_2_Pin);
		  HAL_GPIO_TogglePin(LED_5_GPIO_Port,LED_5_Pin);
		  LED5=HAL_GPIO_ReadPin(LED_5_GPIO_Port, LED_5_Pin);
		  HAL_Delay(speed);
		  HAL_GPIO_TogglePin(LED_1_GPIO_Port,LED_1_Pin);
		  LED1=HAL_GPIO_ReadPin(LED_1_GPIO_Port, LED_1_Pin);
		  HAL_GPIO_TogglePin(LED_6_GPIO_Port,LED_6_Pin);
		  LED6=HAL_GPIO_ReadPin(LED_6_GPIO_Port, LED_6_Pin);
		  HAL_Delay(speed);

		  HAL_GPIO_TogglePin(LED_1_GPIO_Port,LED_1_Pin);
		  LED1=HAL_GPIO_ReadPin(LED_1_GPIO_Port, LED_1_Pin);
		  HAL_Delay(speed);
		  HAL_GPIO_TogglePin(LED_2_GPIO_Port,LED_2_Pin);
		  LED2=HAL_GPIO_ReadPin(LED_2_GPIO_Port, LED_2_Pin);
		  HAL_Delay(speed);
		  HAL_GPIO_TogglePin(LED_3_GPIO_Port,LED_3_Pin);
		  LED3=HAL_GPIO_ReadPin(LED_3_GPIO_Port, LED_3_Pin);
		  HAL_Delay(speed);
		  HAL_GPIO_TogglePin(LED_4_GPIO_Port,LED_4_Pin);
		  LED4=HAL_GPIO_ReadPin(LED_4_GPIO_Port, LED_4_Pin);
		  HAL_Delay(speed);
		  HAL_GPIO_TogglePin(LED_5_GPIO_Port,LED_5_Pin);
		  LED5=HAL_GPIO_ReadPin(LED_5_GPIO_Port, LED_5_Pin);
		  HAL_Delay(speed);
		  HAL_GPIO_TogglePin(LED_6_GPIO_Port,LED_6_Pin);
		  LED6=HAL_GPIO_ReadPin(LED_6_GPIO_Port, LED_6_Pin);
		  HAL_Delay(speed);
	  }




    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 96;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV8;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV4;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOG, LED_1_Pin|LED_2_Pin|LED_3_Pin|LED_4_Pin
                          |LED_5_Pin|LED_6_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : BUTTON_Pin */
  GPIO_InitStruct.Pin = BUTTON_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(BUTTON_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LED_1_Pin LED_2_Pin LED_3_Pin LED_4_Pin
                           LED_5_Pin LED_6_Pin */
  GPIO_InitStruct.Pin = LED_1_Pin|LED_2_Pin|LED_3_Pin|LED_4_Pin
                          |LED_5_Pin|LED_6_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
