# LED_SEQUENCE


## Author
William Alexander Bravo Castro.

## Name
LED SEQUENCE USING PUSH BUTTON

## Description
This project was created to test the debugging and basic coding of the STM32 Cube IDE, the function was created to make a sequence of LEDs controlled by a push button, this sequence has the steps of starting, stopping and changing the speed of the sequence each time press a button.

## Note 
you have to keep the button pressed until this step starts.




